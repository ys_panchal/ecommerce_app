var mongoose = require('mongoose');

var ProductSchema = new mongoose.Schema({
  title: String,
  categories: Array,
  price: Number
});

module.exports = mongoose.model('Product', ProductSchema);