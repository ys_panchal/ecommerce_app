var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Product = require('../models/Product.js');


/* Create new Product */
router.post('/create', function(req, res, next) {
    Product.create(req.body, function (err, product) {
      if (err) return next(err);
      res.json(product);
    });
  });


/* Get Single Product By Id */
router.get('/:id', function(req, res, next) {
    Product.findById(req.params.id, function (err, product) {
      if (err) return next(err);
      res.json(product);
    });
  });


  /* Update Product For Given Id */
router.put('/:id', function(req, res, next) {
    Product.findByIdAndUpdate(req.params.id, req.body, function (err, product) {
      if (err) return next(err);
      res.json(product);
    });
  });


/* Get All Products For Given Category */
/* If Category is Not Given Return All Products */
router.get('/', function(req, res, next) {
    var query = require('url').parse(req.url,true).query;
    var category = query.category;

    if(category){
        var filter_by_category = {"categories": category}
    } else {
        var filter_by_category = {}
    }
    
    Product.find(filter_by_category, function(err, products) {
        if(err) return next(err);
        res.json(products)
    })
 
});

module.exports = router;