var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/ecomm_app', { useNewUrlParser: true }).then(() =>  console.log('Mongodb connection succesful')).catch((err) => console.error(err));

var Category = require('./models/Category.js');
var Product = require('./models/Product.js');

var categories = [
    {
        "name": "Women",
        "child_categories": ["Tops", "Shirts", "Tshirt"]
    },
    {
        "name": "Men",
        "child_categories": ["Jeans", "Trousers", "Shorts"]
    }
]

Category.insertMany(categories, function(err) {
    if(err){
        console.log("Error in category insert ", err)
    } else {
        console.log("Categories Created")
    }
});

var products = [
    {
        "title": "Womens Top 1",
        "categories": ["Women", "Tops"],
        "price": 100
    },
    {
        "title": "Womens Shirt 1",
        "categories": ["Women", "Shirts"],
        "price": 200
    },
    {
        "title": "Womens Tshirt 1",
        "categories": ["Women", "Tshirt"],
        "price": 300
    },
    {
        "title": "Mens Jeans 1",
        "categories": ["Men", "Jeans"],
        "price": 100
    },
    {
        "title": "Mens Trousers 1",
        "categories": ["Men", "Trousers"],
        "price": 200
    },
    {
        "title": "Mens Shorts 1",
        "categories": ["Men", "Shorts"],
        "price": 300
    }
]

Product.insertMany(products, function(err) {
    if(err){
        console.log("Error in product insert ", err)
    } else {
        console.log("Products Created")
        console.log("\n Press Ctrl + C to Exit \n")
    }
});

