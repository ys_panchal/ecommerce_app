var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Category = require('../models/Category.js');

/* GET ALL categories */
router.get('/', function(req, res, next) {
  Category.find(function (err, categories) {
    if (err) return next(err);
    res.json(categories);
  });
});

/* Create new Category */
router.post('/create', function(req, res, next) {
    Category.create(req.body, function (err, category) {
      if (err) return next(err);
      res.json(category);
    });
  });

module.exports = router;