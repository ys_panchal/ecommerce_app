var mongoose = require('mongoose');

var CategorySchema = new mongoose.Schema({
  name: String,
  child_categories: Array
});

module.exports = mongoose.model('Category', CategorySchema);